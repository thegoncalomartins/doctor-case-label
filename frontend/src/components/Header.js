import React, { useState } from 'react';
import { Navbar, Nav, Form, InputGroup, FormControl, Button, Spinner, Alert } from 'react-bootstrap';
import logo from '../logo.svg';
import * as jwt from 'jsonwebtoken';
import * as authService from '../services/AuthService';

const Header = ({ csrfToken, token, setToken, loggedIn, setLoggedIn }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [loading, setLoading] = useState(false);
    const [show, setShow] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');

    const login = (event) => {
        event.preventDefault();

        setLoading(true);

       authService.login({ email, password })
            .then(data => {
                setToken(data.token);
                const decodedToken = jwt.decode(data.token);
                setName(decodedToken.name);
                setLoggedIn(true);
                setPassword('');
                setEmail('');
                setLoading(false);
                setErrorMessage('');
            }).catch(err => {
                err.message && setErrorMessage(err.message);
                setShow(true);
                setPassword('');
                setEmail('');
                setLoading(false);
                setTimeout(() => setShow(false), 2000);
            });
    };

    const logout = (event) => {
        event.preventDefault();

        setLoading(true);

       authService.logout()
            .then(() => {
                setToken('');
                setName('');
                setLoggedIn(false);
                setLoading(false);
            }).catch(err => {
                setToken('');
                setName('');
                setLoggedIn(false);
                setLoading(false);
            });
    };

    return (
        <Navbar bg="dark" variant="dark" expand="lg">
            <Navbar.Brand href="#home">
                <img
                    alt=""
                    src={logo}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                />{' '}
            Doctor-Case-Label
          </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="justify-content-end" style={{ width: '100%' }}>
                    {
                        show ?
                            <Alert variant="danger">
                                An error occurred: {errorMessage}
                            </Alert>
                            :
                            (loading ?
                                <Spinner className="mr-sm-5" animation="border" variant="light" role="status">
                                    <span className="sr-only">Loading...</span>
                                </Spinner>
                                :
                                (
                                    loggedIn ?
                                        <>
                                            <Navbar.Text className="mr-sm-4">
                                                Logged in as: <a href="#home">{name}</a>
                                            </Navbar.Text>
                                            <Button
                                                onClick={logout}
                                                className="mr-sm-4"
                                                variant="outline-info"
                                                type="button"
                                            >
                                                Logout
                                     </Button>
                                        </>
                                        :
                                        <Form onSubmit={login} inline>
                                            <InputGroup>
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <FormControl
                                                    required
                                                    onChange={(event) => setEmail(event.target.value)}
                                                    className="mr-sm-4"
                                                    placeholder="Email"
                                                    aria-label="Email"
                                                    aria-describedby="basic-addon1"
                                                />
                                            </InputGroup>
                                            <FormControl required onChange={(event) => setPassword(event.target.value)} type="password" placeholder="Password" className="mr-sm-4" />
                                            <Button variant="outline-info" type="submit">Login</Button>
                                        </Form>
                                )
                            )
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
};

export default Header;