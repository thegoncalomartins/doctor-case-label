import React, { useEffect, useState } from 'react';
import { Form, Container, Row, Col, Button, Spinner } from 'react-bootstrap';
import * as conditionService from '../services/ConditionService';
import * as caseService from '../services/CaseService';
import * as decisionService from '../services/DecisionService';

const Decision = () => {
    const [conditions, setConditions] = useState([]);
    const [_case_, setCase] = useState({});
    const [conditionId, setConditionId] = useState('');
    const [load, setLoad] = useState(true);
    const [loading, setLoading] = useState(false);
    const [done, setDone] = useState(false);

    useEffect(() => {
        conditionService.getConditions()
            .then(data => setConditions(data.items));
    }, []);

    useEffect(() => {
        if (load) {
            setLoading(true);
            caseService.getCases()
                .then(data => {
                    if (data.items.length) {
                        setCase(data.items[0]);
                    } else {
                        setDone(true);
                    }
                    setLoad(false);
                    setLoading(false);
                });
        }
    }, [load]);

    const submit = (event) => {
        event.preventDefault();

        const decision = {
            case_id: _case_.id,
            condition_id: conditionId
        };

        setLoading(true);
        decisionService.createDecision(decision)
            .then(() => {
                setLoad(true);
                setLoading(false);
            });
    };

    return (
        loading ?
            <div className="d-flex justify-content-center align-items-center" style={{ 'minHeight': '100vh' }}>
                <Spinner className="mr-sm-5" animation="border" variant="dark" role="status">
                    <span className="sr-only"> Loading...</span >
                </Spinner >
            </div>
            :
            (done ?
                <div className="d-flex justify-content-center align-items-center" style={{ 'minHeight': '100vh' }}>
                    <p>
                        You are Done
        </p>
                </div> :
                <Container fluid className="mt-sm-4">
                    <Form onSubmit={submit}>
                        <Row>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Please Review This Case</Form.Label>
                                    <Form.Control readOnly as="textarea" rows={32} value={_case_.description} />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group>
                                    <Form.Label>Select Condition</Form.Label>
                                    <select onChange={(event) => setConditionId(event.target.value)} value={conditionId} className="mt-sm-5 form-control" size="20">
                                        {conditions.map((cond, idx) =>
                                            <option key={idx} value={cond.id}>{`${cond.icd_10_description} ( ${cond.icd_10} )`}</option>
                                        )}
                                    </select>
                                </Form.Group>
                                <div className="d-flex justify-content-end" style={{ width: '100%' }}>
                                    <Button variant="outline-info" type="submit">Next Case</Button>
                                </div>
                            </Col>
                        </Row>
                    </Form>
                </Container>
            )
    );
};

export default Decision;