import axios from 'axios';

export function createDecision(decision) {
    return axios.post('/api/v1/decisions', decision)
        .then(({ data }) => JSON.parse(data.replace('while(1);', '')))
        .catch(err => {
            let data = err.response?.data || '';
            data = JSON.parse(data.replace('while(1);', ''));

            throw new Error(data.error);
        });
};