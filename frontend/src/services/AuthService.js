import axios from 'axios';

export function login({ email, password }) {
    return axios.post('/api/v1/auth/login', { email, password })
        .then(({ data }) => JSON.parse(data.replace('while(1);', '')))
        .catch(err => {
            let data = err.response?.data || '';
            data = JSON.parse(data.replace('while(1);', ''));

            throw new Error(data.error);
        });
};

export function logout() {
    return axios.post('/api/v1/auth/logout');
}