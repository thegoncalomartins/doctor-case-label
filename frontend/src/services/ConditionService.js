import axios from 'axios';

export function getConditions() {
    return axios.get('/api/v1/conditions')
        .then(({ data }) => JSON.parse(data.replace('while(1);', '')))
        .catch(err => {
            let data = err.response?.data || '';
            data = JSON.parse(data.replace('while(1);', ''));

            throw new Error(data.error);
        });
};