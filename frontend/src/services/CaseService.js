import axios from 'axios';

export function getCases() {
    return axios.get('/api/v1/cases')
        .then(({ data }) => JSON.parse(data.replace('while(1);', '')))
        .catch(err => {
            let data = err.response?.data || '';
            data = JSON.parse(data.replace('while(1);', ''));

            throw new Error(data.error);
        });
};