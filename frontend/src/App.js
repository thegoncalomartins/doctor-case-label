import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from 'react';
import Logo from './components/Logo';
import Header from './components/Header';
import Decision from './components/Decision';
import axios from 'axios';

const App = () => {
  const [token, setToken] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);

  useEffect(() => {
    axios.get('/csrf-token')
      .then(({ data }) => axios.defaults.headers.post['X-CSRF-Token'] = data.csrfToken);
  }, []);

  return (
    <>
      <Header
        token={token}
        setToken={setToken}
        loggedIn={loggedIn}
        setLoggedIn={setLoggedIn}
      />
      {
        loggedIn ?
          <Decision />
          :
          <Logo />
      }
    </>
  );
}

export default App;
