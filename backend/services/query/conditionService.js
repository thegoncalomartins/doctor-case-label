const mongodb = require('mongodb');
const mongoClientHelper = require('../../helpers/db/mongoClientHelper');
const mongoQueryHelper = require('../../helpers/db/mongoQueryHelper');
const dbName = process.env.DB_NAME || 'doctor-case'; // ...we may want to have a db for each client (e.g. schema-per-tenant/database-per-tenant approach)
const conditionRepository = require('../../repositories/query/conditionRepository');
const debug = require('debug')('conditionService');

const minLimit = 122;
const maxLimit = 200;

const toDTO = (condition) => ({id: condition._id, icd_10: condition.ICD_10, icd_10_description: condition.ICD_10_Description});

module.exports = {
    findAll: async ({ limit = '1', page = '1' }) => {
        page = mongoQueryHelper.calculatePage(page);
        limit = mongoQueryHelper.calculateLimit(minLimit, maxLimit, limit);
        const offset = mongoQueryHelper.calculateOffset(page, limit);

        const client = await mongoClientHelper.connect();

        try {
            const count = await conditionRepository.count(client, dbName);
            const conditions = (await conditionRepository.findAll(client, dbName, limit, offset))
                .map(toDTO);

            return {
                total: count,
                limit: limit,
                page: page,
                items: conditions
            };
        } catch (error) {
            debug('findAll error: %s', error.message);
            throw error;
        } finally {
            client.close();
        }
    }
};