const mongodb = require('mongodb');
const mongoClientHelper = require('../../helpers/db/mongoClientHelper');
const mongoQueryHelper = require('../../helpers/db/mongoQueryHelper');
const dbName = process.env.DB_NAME || 'doctor-case'; // ...we may want to have a db for each client (e.g. schema-per-tenant/database-per-tenant approach)
const caseRepository = require('../../repositories/query/caseRepository');
const debug = require('debug')('caseService');

const minLimit = 1;
const maxLimit = 1;
const minPage = 1;

const toDTO = (_case_) => ({id: _case_._id, description: _case_.description});

module.exports = {
    findAll: async ({ limit = '1', page = '1' }) => {
        page = mongoQueryHelper.calculatePage(page);
        limit = mongoQueryHelper.calculateLimit(minLimit, maxLimit, limit);
        const offset = mongoQueryHelper.calculateOffset(page, limit);

        const client = await mongoClientHelper.connect();

        try {
            const cases = (await caseRepository.findAll(client, dbName, limit, offset)).map(toDTO);
            const count = await caseRepository.count(client, dbName);

            return {
                total: count,
                limit: limit,
                page: page,
                items: cases
            };
        } catch (error) {
            debug('findAll error: %s', error.message);
            throw error;
        } finally {
            client.close();
        }
    }
};