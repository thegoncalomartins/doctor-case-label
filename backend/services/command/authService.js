const mongoClientHelper = require('../../helpers/db/mongoClientHelper');
const dbName = process.env.DB_NAME || 'doctor-case'; // ...we may want to have a db for each client (e.g. schema-per-tenant/database-per-tenant approach)
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const secret = process.env.SECRET || 'secret';
const userRepository = require('../../repositories/command/userRepository');
const tokenBlacklistRepository = require('../../repositories/command/tokenBlacklistRepository');
const debug = require('debug')('authService');

module.exports = {
    login: async (email, password) => {
        const client = await mongoClientHelper.connect();
        const session = mongoClientHelper.startSession(client);

        try {
            const user = await userRepository.findOne(client, dbName, session, email);

            if (!user) {
                throw new Error('Invalid credentials');
            }

            const userPassword = user.password;

            const match = await bcrypt.compare(password, userPassword);

            if (!match) {
                throw new Error('Invalid credentials');
            }

            const token = jwt.sign({ id: user._id, email: user.email, name: user.name }, secret, { expiresIn: process.env.TOKEN_TIMEOUT || '2h' });

            return token;
        } catch (error) {
            debug('login error: %s', error.message);
            throw error;
        } finally {
            await session.endSession();
            client.close();
        }
    },
    logout: async (token) => {
        const client = await mongoClientHelper.connect();
        const session = mongoClientHelper.startSession(client);

        session.startTransaction();

        try {
            await tokenBlacklistRepository.insert(client, dbName, session, token);

            await session.commitTransaction();
        } catch (error) {
            debug('logout error: %s', error.message);
            await session.abortTransaction();
            throw error;
        } finally {
            await session.endSession();
            client.close();
        }
    }
};