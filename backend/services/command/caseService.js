const caseRepository = require('../../repositories/command/caseRepository');

module.exports = {
    exists: (client, dbName, session, caseId) => {
        return caseRepository.exists(client, dbName, session, caseId);
    }
};