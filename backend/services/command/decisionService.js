const mongodb = require('mongodb');
const mongoClientHelper = require('../../helpers/db/mongoClientHelper');
const dbName = process.env.DB_NAME || 'doctor-case'; // ...we may want to have a db for each client (e.g. schema-per-tenant/database-per-tenant approach)
const decisionRepository = require('../../repositories/command/decisionRepository');
const conditionService = require('./conditionService');
const caseService = require('./caseService');
const debug = require('debug')('decisionService');

module.exports = {
    insert: async (userId, {case_id, condition_id}) => {
        const decision = {
            doctor_id: userId,
            condition_id: condition_id,
            case_id: case_id,
            created_at: Date.now()
        };

        const client = await mongoClientHelper.connect();
        const session = mongoClientHelper.startSession(client);

        session.startTransaction();

        try {
            if ((await decisionRepository.exists(client, dbName, session, case_id))) {
                throw new Error(`Decision already exists for case with id ${case_id}`);
            }

            if (!(await caseService.exists(client, dbName, session, case_id))) {
                throw new Error(`Case with id ${case_id} doesn't exist`);
            }

            if(!(await conditionService.exists(client, dbName, session, condition_id))) {
                throw new Error(`Condition with id ${condition_id} doesn't exist`);
            }

            const result = await decisionRepository.insert(client, dbName, session, decision);

            await session.commitTransaction();

            return result.insertedId;
        } catch (error) {
            debug('insert error: %s', error.message);
            await session.abortTransaction();
            throw error;
        } finally {
            await session.endSession();
            client.close();
        }
    }
};