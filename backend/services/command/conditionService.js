const conditionRepository = require('../../repositories/command/conditionRepository');

module.exports = {
    exists: (client, dbName, session, conditionId) => {
        return conditionRepository.exists(client, dbName, session, conditionId);
    }
};