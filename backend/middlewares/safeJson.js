// https://github.com/helmetjs/express-json-hijack-prevention
module.exports = (options) => {
    options = options || {};
    const prefix = options.prefix || 'while(1);';

    return (req, res, next) => {
        res.safejson = (value) => res.send(`${prefix}${JSON.stringify(value)}`);
        next();
    };
};