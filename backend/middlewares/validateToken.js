const jwt = require('jsonwebtoken');
const secret = process.env.SECRET || 'secret';
const mongoClientHelper = require('../helpers/db/mongoClientHelper');
const dbName = process.env.DB_NAME || 'doctor-case'; // ...we may want to have a db for each client (e.g. schema-per-tenant/database-per-tenant approach)
const debug = require('debug')('middleware:validateToken');
const tokenBlacklistRepository = require('../repositories/command/tokenBlacklistRepository');

// an alternative would be to use the 'express-jwt' package to implement the middleware
// https://www.npmjs.com/package/express-jwt

module.exports = {
    validateToken: (req, res, next) => {
        let token = req.headers.authorization || req.cookies.token;

        if (!token) {
           return res.status(401).json({error: 'Unauthenticated'});
        }

        token = token.replace('Bearer ', '');

        jwt.verify(token, secret, { clockTimestamp: Date.now() / 1000}, async (err, decoded) => {
            if (err) {
                return res.status(401).json({error: 'Invalid or expired token'});
            }

            const client = await mongoClientHelper.connect();
            const session = mongoClientHelper.startSession(client);
            
            try {
                const tokenInBlacklist = await tokenBlacklistRepository.findOne(client, dbName, session, token);

                if (tokenInBlacklist) {
                    throw new Error('Token already blacklisted');
                }
            } catch (error) {
                debug('error: %s', error.message);
                return res.status(401).json({error: error.message});
            } finally {
                await session.endSession();
                client.close();
            }

            // inject token properties directly in the request to be used next
            req.token = token;
            req.userId = decoded.id;
            req.userEmail = decoded.email;
            req.userName = decoded.name;
            next();
        });
    }
};