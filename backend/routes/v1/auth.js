const express = require('express');
const router = express.Router();
const authService = require('../../services/command/authService');
const { validateToken } = require('../../middlewares/validateToken');

router.post('/login', (req, res) => {
    const { email, password } = req.body;

    authService
        .login(email, password)
        .then(token => res.status(200).cookie('token', token, { httpOnly: true, maxAge: 2 * 3600 * 1000 }).safejson({ token: token }))
        .catch(err => res.status(401).safejson({ error: err.message }));
});

router.post('/logout', validateToken, (req, res) => {
    const token = req.token;

    authService
        .logout(token)
        .then(() => res.status(204).clearCookie('token').send())
        .catch(err => res.status(401).safejson({ error: err.message }));
});

module.exports = router;