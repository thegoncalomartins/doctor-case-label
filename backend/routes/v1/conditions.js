const express = require('express');
const router = express.Router();
const conditionService = require('../../services/query/conditionService');
const { validateToken } = require('../../middlewares/validateToken');

router.get('/', validateToken, (req, res) => {
    conditionService
        .findAll(req.query)
        .then(conditions => res.status(200).safejson(conditions))
        .catch(err => res.status(500).safejson({ error: err.message }));
});

module.exports = router;