const express = require('express');
const router = express.Router();

const authRouter = require('./auth');
const conditionsRouter = require('./conditions');
const decisionsRouter = require('./decisions');
const casesRouter = require('./cases');

router.use('/auth', authRouter);
router.use('/conditions', conditionsRouter);
router.use('/decisions', decisionsRouter);
router.use('/cases', casesRouter);

module.exports = router;