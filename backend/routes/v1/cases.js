const express = require('express');
const router = express.Router();
const caseService = require('../../services/query/caseService');
const { validateToken } = require('../../middlewares/validateToken');

router.get('/', validateToken, (req, res) => {
    caseService
        .findAll(req.query)
        .then(cases => res.status(200).safejson(cases))
        .catch(err => res.status(500).safejson({ error: err.message }));
});

module.exports = router;