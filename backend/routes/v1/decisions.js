const express = require('express');
const router = express.Router();
const decisionService = require('../../services/command/decisionService');
const { validateToken } = require('../../middlewares/validateToken');

router.post('/', validateToken, (req, res) => {
    decisionService
        .insert(req.userId, req.body)
        .then(decisionId => res.status(201).location(`${req.originalUrl}/${decisionId}`).safejson({id: decisionId}))
        .catch(err => res.status(500).safejson({ error: err.message }));
});

module.exports = router;