const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const csrf = require('csurf');
const safeJson = require('./middlewares/safeJson');
require('dotenv').config()
const apiRouter = require('./routes/api');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(safeJson());
app.use(csrf({
  cookie: true
}));
// Serve static files from the React frontend app
app.use(express.static(path.join(__dirname, '../frontend/build')));

app.use('/api', apiRouter);

// https://medium.com/@ryanchenkie_40935/react-authentication-how-to-store-jwt-in-a-cookie-346519310e81
app.get('/csrf-token', (req, res) => {
  res.json({ csrfToken: req.csrfToken() });
});

// Anything that doesn't match the above, send back index.html
app.get('*', (req, res) => res.sendFile(path.join(__dirname, '../frontend/build/index.html')));

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500).json({error: err.message});
});

module.exports = app;
