const mongodb = require('mongodb');
const url = process.env.DB_URL || 'mongodb://root:5Xd9YU5F9QIdNMvz@localhost:27017';
const transactionOptions = {
    readPreference: 'primary',
    readConcern: { level: 'local' },
    writeConcern: { w: 'majority' }
};
const sessionOptions = {
    defaultTransactionOptions: transactionOptions
}

module.exports = {
    connect: () => mongodb.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }),
    startSession: (client) => client.startSession(sessionOptions)
}