const minPage = 1;

module.exports = {
    calculatePage: (page) => {
        page = Math.max(minPage, parseInt(page, 10));

        return page;
    },
    calculateLimit: (minLimit, maxLimit, limit) => {
        limit = parseInt(limit, 10);
        limit = Math.min(maxLimit, Math.max(minLimit, limit));

        return limit;
    },
    calculateOffset: (page, limit) => {
        page = Math.max(minPage, parseInt(page, 10));
        const offset = page * limit - limit;

        return offset;
    }
};