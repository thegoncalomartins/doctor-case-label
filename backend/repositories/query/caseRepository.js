const collectionName = 'case';
const findAllPipeline = [
    {
        '$lookup': {
            'from': 'decision',
            'localField': '_id',
            'foreignField': 'case_id',
            'as': 'decision'
        }
    }, {
        '$unwind': {
            'path': '$decision',
            'preserveNullAndEmptyArrays': true
        }
    }, {
        '$match': {
            'decision': {
                '$exists': false
            }
        }
    }
];

const countPipeline = [
    ...findAllPipeline,
    {
        '$count': 'count'
    }
];

module.exports = {
    findAll: (client, dbName, limit = 1, offset = 0) => {
        const collection = client.db(dbName).collection(collectionName);
        const pipeline = [
            ...findAllPipeline,
            {
                '$skip': offset
            },
            {
                '$limit': limit
            }
        ];

        return collection.aggregate(pipeline).toArray();
    },
    count: async (client, dbName) => {
        const collection = client.db(dbName).collection(collectionName);

        const result = await collection.aggregate(countPipeline).toArray();

        if (result.length) {
            return result[0].count;
        }

        return 0;
    }
};