const collectionName = 'condition';

module.exports = {
    findAll: (client, dbName, limit = 122, offset = 0) => {
        const collection = client.db(dbName).collection(collectionName);

        return collection.find({}).skip(offset).limit(limit).toArray();
    },
    count: (client, dbName) => {
        const collection = client.db(dbName).collection(collectionName);

        return collection.countDocuments();
    }
};