const { uuid } = require('uuidv4');
const collectionName = 'condition';

module.exports = {
    exists: async (client, dbName, session, conditionId) => {
        const collection = client.db(dbName).collection(collectionName);

        return Boolean(await collection.findOne({ _id: conditionId }, { session }));
    }
};