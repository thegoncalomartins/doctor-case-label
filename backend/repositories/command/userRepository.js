const collectionName = 'user';

module.exports = {
    findOne: async (client, dbName, session, email) => {
        const collection = client.db(dbName).collection(collectionName);

        return await collection.findOne({email: email}, {session});
    }
};