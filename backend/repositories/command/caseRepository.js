const { uuid } = require('uuidv4');
const collectionName = 'case';

module.exports = {
    exists: async (client, dbName, session, caseId) => {
        const collection = client.db(dbName).collection(collectionName);

        return Boolean(await collection.findOne({ _id: caseId }, { session }));
    }
};