const { uuid } = require('uuidv4');
const collectionName = 'decision';

module.exports = {
    exists: async (client, dbName, session, caseId) => {
        const collection = client.db(dbName).collection(collectionName);

        return Boolean(await collection.findOne({ case_id: caseId }, { session }));
    },
    insert: (client, dbName, session, decision) => {
        const collection = client.db(dbName).collection(collectionName);

        return collection.insertOne({ _id: uuid(), ...decision }, { session });
    }
};