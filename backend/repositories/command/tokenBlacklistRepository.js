const collectionName = 'token_blacklist';

module.exports = {
    findOne: (client, dbName, session, token) => {
        const collection = client.db(dbName).collection(collectionName);

        return collection.findOne({_id: token}, {session});
    },
    insert: (client, dbName, session, token) => {
        const collection = client.db(dbName).collection(collectionName);

        return collection.insertOne({_id: token}, {session});
    }
};