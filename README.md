# README #
## [**Challenge**](https://docs.google.com/document/d/1HcJTK6T6C5uJAaT3CnB4_9diaf_ozXU3_7SyNC0YF-0/edit) ##

**Background**

To obtain training data for  our ML-based diagnosis, Gyant ask doctors to label medical cases by reading the EHR (electronic health record) of a patient encounter and labeling it with a diagnosis (condition).

An EHR is a text file that may look something like this:
> The patient is a 32 year old female who presents to the Urgent Care complaining of sore throat that started 7 days ago. Developed into post nasal drip and then cough. No measured fevers. Had chills and body aches at the onset that resolved after the first day. A little facial headache with the congestion at times; better today. Some pressure on and off in the ears, no pain, hearing loss or tinnitus. Cough is mostly dry, sometimes productive of clear sputum. Denies shortness of breath. Never smoker. Has never needed inhalers. No history of pneumonia. Currently treating with ibuprofen which helps. Tried some over-the-counter Mucinex ES and vitamin C.

A condition consists of an ICD-10 condition code and description, e.g.:
> F411    Generalized anxiety disorder

> J00     Acute nasopharyngitis

**Assignment**

Create a web application that allows a doctor to review the EHR (one after another) and label it with one of a number of conditions. The case id, doctor id, label and time to label the case should be recorded for each decision.

**Data**

Please find the list of conditions and sample cases [here](https://drive.google.com/drive/folders/1dANQcGwJ0zjqRWH1PHbqxDrxOoCQGT1A).

**Technology**

* Back-end: Node.js (Nest.js or similar) and MongoDB
* Front-end: React

Optionally, Typescript can be used.

**Use Case**

* Doctor logs in using email/password (you can create a mock account in the DB)
* The next case is presented
* Doctor labels the case and moves on to the next one
* When no more cases are left, a message "You are Done" is displayed
* Doctor can log out and re-log in at any point


### UI Mockup ###
![UI Mockup](ui-mockup.png)

## Documentation ##
### Entity Relationship Diagram (ERD) ###
![ERD Diagram](erd-diagram.png)

The **password** field in the **user** collection stores the user's password hash ([bcrypt](https://www.npmjs.com/package/bcrypt))

### Component/Deployment Diagram ###
![Component/Deployment Diagram](component-diagram.png)
### Used technologies and dependencies ###

* Frontend
    * Framework
        * [React](https://reactjs.org/)
    * Dependencies
        * [bootstrap](https://www.npmjs.com/package/bootstrap)
        * [react-bootstrap](https://www.npmjs.com/package/react-bootstrap)
        * [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
        * [axios](https://www.npmjs.com/package/axios)
* Backend
    * Framework
        * Node.js ([Express](https://www.npmjs.com/package/express))
    * Dependencies
        * [bcrypt](https://www.npmjs.com/package/bcrypt)
        * [cookie-parser](https://www.npmjs.com/package/cookie-parser)
        * [csurf](https://www.npmjs.com/package/csurf)
        * [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
        * [mongodb](https://www.npmjs.com/package/mongodb)
## Getting started ##
### How to run ###

#### **Locally** ####

Make sure you have [`node`](https://nodejs.org/en/), [`docker`](https://docs.docker.com/get-docker/) and [`docker-compose`](https://docs.docker.com/compose/install/) installed.

Run `docker-compose up -d`

After that you can access the database at `mongodb://root:5Xd9YU5F9QIdNMvz@localhost:27017`

You have to manually create the database (`doctor-case-label`) and the collections described in the ERD diagram (import the csv files present in this directory). You can use [MongoDB Compass](https://www.mongodb.com/try/download/compass), [Robo 3T](https://robomongo.org/download) or any other MongoDB client application.

The password for the *dummy* user in the `users.csv` file is **password**.

You can create your own `.env` file in the backend directory and paste the following properties in it.

E.g.:

* PORT=5000
* DB_URL=mongodb://root:5Xd9YU5F9QIdNMvz@localhost:27017
* DB_NAME=doctor-case-label
* TOKEN_TIMEOUT=2h
* SECRET=secret

Then run 

* `npm run build`
* `npm start`

Open the application at [localhost:5000](http://localhost:5000/)

Optionally you can interact only with the backend, for that, it was prepared a postman collection which is in the root of this repository. Import it in [Postman](https://www.postman.com/downloads/) and you're ready to go.

#### **Production** ####

Just [open the application (doctor-case-label.herokuapp.com)](https://doctor-case-label.herokuapp.com)

## Demo ##

![Demo](demo.gif)

## Future work / Improvements ##

* Add internationalization and localization to frontend
* Logs in both applications
* Customized error messages/codes from backend when the requests return error
* Monitorization
* Implement functional/end-2-end tests (with [Selenium](https://www.selenium.dev/), [Cypress](https://www.cypress.io/) or [Puppeteer](https://pptr.dev/)) and integrate them in the pipeline
* Implement unit and integration tests ([jest](https://jestjs.io/), [mocha](https://mochajs.org/), ...) in the backend and frontend and integrate them in the pipeline
* Use OAuth2.0 (OIDC) with social login from Google or Facebook or another IAM (Identity and Access Management) software/platform like [Okta](https://www.okta.com/) or [Keycloak](https://www.keycloak.org/)
* Add an API gateway (like [Kraken'd](https://www.krakend.io/) or [Kong](https://konghq.com/kong/)) to handle the authorization and other questions like load balancement, rate limits to prevent DDoS attacks, etc
* Add Open-API / Swagger-UI endpoint to the backend
* Implement Hypermedia in the backend API